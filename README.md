# PhysNTupleDumper

A simple n-tuple dumper using CP algorithms to run on PHYS/PHYSLITE.

This is a fork of the orginal code by Nils Krumnack at https://gitlab.cern.ch/krumnack/physntupledumper and documented at https://gitlab.cern.ch/krumnack/physntupledumper/-/blob/master/HandsOn.md - a few small fixes have been added to the orginial code.

## Getting started

General setup of ATLAS environment:
```
seetupATLAS
lsetup git
```
Prepare the directory structure and checkout the code:
```
mkdir physntupledumper
cd physntupledumper
mkdir source build run
cd source
git clone https://gitlab.cern.ch/elmsheus/physntupledumper.git
cp /afs/cern.ch/user/e/elmsheus/public/CMakeLists.txt .
```
The last line copies a generic top-level ATLAS `CMakeLists.txt`, as documented at:[https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/](https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/)

Setup the latest `AnalysisBase` nightly release from CVMFS and compile the code:
```
cd ../build
asetup AnalysisBase,master,latest
cmake ../source/
make
source x86_64-centos7-gcc11-opt/setup.sh
```
Run the code over 500 events with an example MC and data DAOD_PHYSLITE file located on CVMFS:
```
cd ../run
PhysNTupleDumper_eljob.py --max-events 500 --data-type mc --force-input $ASG_TEST_FILE_LITE_MC --no-physlite-broken --physlite > out_mc.txt 2>&1 &
PhysNTupleDumper_eljob.py --max-events 500 --data-type data --force-input $ASG_TEST_FILE_LITE_DATA --no-physlite-broken --physlite > out_data.txt 2>&1 &
```
The output in the `submitDir*` subdirectories.

