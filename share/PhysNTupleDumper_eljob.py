#!/usr/bin/env python
#
# Copyright (C) 2002-2022  CERN for the benefit of the ATLAS collaboration
#
# @author Nils Krumnack


# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--data-type', default='data',
                   help='Type of data to run over. Valid options are data, mc, afii')
parser.add_argument('--physlite', dest='physlite', action='store_true',
                    help='Configure the job for physlite')
parser.add_argument('--no-physlite-broken', action='store_true',
                   help='Configure the job to skip algorithms that fail on physlite test file')
parser.add_argument('-s', '--submission-dir',
                   action='store', default='submitDir',
                   help='Submission directory for EventLoop')
parser.add_argument('--max-events', type=int, help='Number of events to run')
parser.add_argument('-u', '--unit-test', action='store_true',
                   help='Run the job in "unit test mode"')
parser.add_argument("--force-input", help="Force the given input file")
parser.add_argument('--algorithm-timer', action='store_true',
                   help='Run the job with a timer for each algorithm')
parser.add_argument('--no-systematics', action='store_true',
                   help='Configure the job to with no systematics')
parser.add_argument('--no-debug-histograms', action='store_true',
                   help='Configure the job to with no debugging histograms')
args = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Force-load some xAOD dictionaries. To avoid issues from ROOT-10940.
ROOT.xAOD.TauJetContainer()

# ideally we'd run over all of them, but we don't have a mechanism to
# configure per-sample right now

dataType = args.data_type

if dataType not in ["data", "mc", "afii"] :
    raise Exception ("invalid data type: " + dataType)

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sample = ROOT.SH.SampleLocal (dataType)
inputfile = {"data": 'ASG_TEST_FILE_DATA',
             "mc":   'ASG_TEST_FILE_MC',
             "afii": 'ASG_TEST_FILE_MC_AFII'}

if args.force_input :
    sample.add (args.force_input)
else :
    sample.add (os.getenv (inputfile[dataType]))
sh.add (sample)

#ROOT.SH.ScanDir().filePattern( '*' ).scan( sh, args.force_input )

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
if args.max_events is not None:
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, args.max_events )
if args.algorithm_timer :
    job.options().setBool( ROOT.EL.Job.optAlgorithmTimer, True )


from PhysNTupleDumper.NTupleDumper import makeSequence
algSeq = makeSequence (dataType, isPhyslite=args.physlite, noPhysliteBroken=args.no_physlite_broken,
                       noSystematics = args.no_systematics,
                       debugHistograms = not args.no_debug_histograms)
print( algSeq ) # For debugging
algSeq.addSelfToJob( job )

# Make sure that both the ntuple and the xAOD dumper have a stream to write to.
job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )

# Find the right output directory:
submitDir = args.submission_dir
if args.unit_test:
    job.options().setString (ROOT.EL.Job.optSubmitDirMode, 'unique')
else :
    job.options().setString (ROOT.EL.Job.optSubmitDirMode, 'unique-link')


driver = ROOT.EL.DirectDriver()

driver.submit( job, submitDir )
