# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# @author Nils Krumnack

# User options, which can be set from command line after a "-" character
# athena NTupleDumper_jobOptions.py - --myOption ...
from AthenaCommon.AthArgumentParser import AthArgumentParser
athArgsParser = AthArgumentParser()
athArgsParser.add_argument("--force-input", help="Force the given input file")
athArgsParser.add_argument("--force-output", help="Force the given output file")
athArgsParser.add_argument("--data-type", default="data",
                           help="Type of input to run over. Valid options are 'data', 'mc', 'afii'")
athArgsParser.add_argument('--physlite', action='store_true', help='Configure the job for physlite')
athArgsParser.add_argument('--no-physlite-broken', action='store_true',
                            help='Configure the job to skip algorithms that fail on physlite test file')
athArgsParser.add_argument('--no-systematics', action='store_true',
                            help='Configure the job to with no systematics')
athArgsParser.add_argument('--no-debug-histograms', action='store_true',
                            help='Configure the job to with no debugging histograms')
athArgs = athArgsParser.parse_args()

dataType = athArgs.data_type
if not dataType in ["data", "mc", "afii"] :
    raise Exception ("invalid data type: " + dataType)

print("Running on data type: " + dataType)

inputfile = {"data": 'ASG_TEST_FILE_DATA',
             "mc":   'ASG_TEST_FILE_MC',
             "afii": 'ASG_TEST_FILE_MC_AFII'}

# Set up the reading of the input file:
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = 500
testFile = os.getenv ( inputfile[dataType] )
if athArgs.force_input :
    testFile = athArgs.force_input
svcMgr.EventSelector.InputCollections = [testFile]

from PhysNTupleDumper.NTupleDumper import makeSequence
algSeq = makeSequence (dataType, isPhyslite = athArgs.physlite,
                       noPhysliteBroken = athArgs.no_physlite_broken,
                       noSystematics = athArgs.no_systematics,
                       debugHistograms = not athArgs.no_debug_histograms)
print (algSeq) # For debugging

# Add all algorithms from the sequence to the job.
athAlgSeq += algSeq

# Set up a histogram output file for the job:
ServiceMgr += CfgMgr.THistSvc()
outputFile = "ANALYSIS DATAFILE='NTupleDumper." + dataType + ".hist.root' OPT='RECREATE'"
if athArgs.force_output :
    outputFile = athArgs.force_output
ServiceMgr.THistSvc.Output += [ outputFile ]

# Reduce the printout from Athena:
include( "AthAnalysisBaseComps/SuppressLogging.py" )
