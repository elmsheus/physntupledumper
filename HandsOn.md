# PHYS-PHYSLITE Hands-On

## Setup Source Area

If you have a source area setup for use with ATLAS cmake and ATLAS
releases (e.g. like done in the software tutorial).  If you don't have
that, follow these instructions:

    mkdir tutorial
    cd tutorial
    mkdir source
    mkdir build
    mkdir run

Inside the source area you need a generic top-level ATLAS
`CMakeLists.txt`, e.g. as found here:
https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/

## Checkout And Build N-Tuple Code

You will need to checkout and compile the n-tuple code:

    cd source
    git clone https://:@gitlab.cern.ch:8443/krumnack/PhysNTupleDumper.git
    cd ../build
    asetup AnalysisBase,22.2.88
    cmake ../source
    make
    source x86*/setup.sh

Note that the n-tuple package (currently) only contains python code,
but the compilation is still needed to put the python files in the
right locations, etc.

You can also use AthAnalysis instead, modify the commands below
accordingly.

## Run The N-Tuple Code

Let's run the n-tuple code twice, once on the PHYS test-file, once on
the PHYSLITE test-file.  Both of these are distributed via cvmfs and
defined in the release:

    PhysNTupleDumper_eljob.py --max-events 500 --data-type mc --force-input $ASG_TEST_FILE_MC --no-physlite-broken
    PhysNTupleDumper_eljob.py --max-events 500 --data-type mc --force-input $ASG_TEST_FILE_LITE_MC --no-physlite-broken --physlite

This will create two submissions directories, with content that should
essentially be identical.  The --no-physlite-broken flag disables some
code that currently doesn't work on PHYSLITE.  The --physlite flag
tells the code that it runs on PHYSLITE, as some settings are
different.

If you chose to run in Athena instead the job options file is called
PhysNTupleDumper_jobOptions.py.

## Look At The Code

Now let's take a look at the code:  https://gitlab.cern.ch:8443/krumnack/physntupledumper/-/blob/master/python/NTupleDumper.py

This is a fairly complete n-tuple dumping code based on the CP
algorithms.  You can go through it, looking out for two flags passed
in:
- isPhyslite: This creates separate code paths for PHYS and PHYSLITE.
  This is mostly changing names that are different in PHYSLITE, but
  some code doesn't need to be run again, since it is already run in
  derivations.
- noPhysliteBroken: This disables code that is currently not working
  on PHYSLITE.  This is mostly due to variables missing in the current
  version of PHYSLITE, that will be present in the next one.
